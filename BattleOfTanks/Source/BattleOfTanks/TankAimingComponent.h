// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TankAimingComponent.generated.h"

class AProjectile;

//Enum For Aiming States
UENUM()
enum class EAimState:uint8
{
	Locked,
	Reloading,
	Aiming
};

//Forward Declaration
class UTankBarrel;
class UTankTurret;
//Holds barrel's properties and Elevate method
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLEOFTANKS_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

public:	
	void AimAt(FVector HitLocation);
	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(UTankBarrel* BarrelToSet, UTankTurret* TurretToSet);
	UFUNCTION(BlueprintCallable,Category="Firing")
		void Fire();
protected:
	UPROPERTY(BlueprintReadOnly , Category = "State")
	EAimState AimState = EAimState::Aiming;
	
private:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	UTankBarrel * Barrel = nullptr;
	UTankTurret* Turret = nullptr;

	UPROPERTY(EditAnywhere, Category = "Setup")
		TSubclassOf<AProjectile> ProjectileBlueprint;
	UPROPERTY(EditAnywhere, Category = "Firing")
		float LaunchSpeed = 5000;//
	UPROPERTY(EditAnywhere, Category = "Firing")
		float ReloadTimeInSeconds = 3.f;
	double LastFireTime = 0;
	FVector AimDirection;
	void MoveBarrelTowards(FVector AimDirection);
	void MoveTurretTowards(FVector AimDirection);
	bool isBarrelMoving();
	
};
