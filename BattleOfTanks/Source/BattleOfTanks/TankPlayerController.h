// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"


/**
 * 
 */
class UTankAimingComponent;

UCLASS()
class BATTLEOFTANKS_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;
protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void AimingComponentFound(UTankAimingComponent* AimCompRef);
private:
	void AimTowardsCrosshair();
	bool GetSightRayHitLocation(FVector& OutHitLocation) const;
	bool GetLookDirection(FVector2D ScreenLocation, FVector & LookDirection) const;
	ATankPlayerController();
	
	UPROPERTY(EditDefaultsOnly)
	float CrosshairXLocation;
	UPROPERTY(EditDefaultsOnly)
	float CrosshairYLocation;
	UPROPERTY(EditDefaultsOnly)
		float LineTraceRange=1000000.f;
	bool GetLookVectorHitLocation(FVector& HitLocation, FVector LookDirection) const;
	FVector GetLineTraceEnd();
};
