// Fill out your copyright notice in the Description page of Project Settings.

#include "TankMovementComponent.h"
#include "TankTrack.h"
void UTankMovementComponent::Initialise(UTankTrack * LeftTrackToSet, UTankTrack * RightTrackToSet)
{
	LeftTrack = LeftTrackToSet;
	RightTrack = RightTrackToSet;
}
void UTankMovementComponent::IntendMoveForwardOrBackward(float Throw)
{
	if (ensure(LeftTrack && RightTrack))
	{
		LeftTrack->SetThrottle(Throw);
		RightTrack->SetThrottle(Throw);
	}
}

void UTankMovementComponent::IntendTurnRight(float Throw)
{
	if (ensure(LeftTrack && RightTrack))
	{
		LeftTrack->SetThrottle(Throw);
		RightTrack->SetThrottle(-Throw);
	}
}

void UTankMovementComponent::IntendTurnLeft(float Throw)
{
	if (ensure(LeftTrack && RightTrack))
	{
		LeftTrack->SetThrottle(-Throw);
		RightTrack->SetThrottle(Throw);
	}
}

void UTankMovementComponent::RequestDirectMove(const FVector & MoveVelocity, bool bForceMaxSpeed)
{
	auto TankForward= GetOwner()->GetActorForwardVector().GetSafeNormal();
	auto AIMoveDirection = MoveVelocity.GetSafeNormal();

	auto ForwardThrow= FVector::DotProduct(TankForward, AIMoveDirection);
	auto TurnThrow = FVector::CrossProduct(TankForward, AIMoveDirection).Z;
	
	IntendMoveForwardOrBackward(ForwardThrow);
	IntendTurnRight(TurnThrow);
	

}


