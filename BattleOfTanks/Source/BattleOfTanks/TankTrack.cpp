// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTrack.h"
#include "Engine/World.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = true;


}
void UTankTrack::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime,TickType,ThisTickFunction);
	//calculate the required acceleration this frame to correct
	auto slippageSpeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
	auto correctionAcceleration = -slippageSpeed / DeltaTime * GetRightVector();

	auto tankRoot=Cast<UStaticMeshComponent>(GetOwner()->GetRootComponent());
	auto correctionForce = tankRoot->GetMass()*correctionAcceleration/2;
	tankRoot->AddForce(correctionForce); 


}
void UTankTrack::SetThrottle(float Throttle)
{
	auto Force = GetForwardVector()*Throttle*TrackMaxDriveForce;
	auto ForceLocation = GetComponentLocation();

	auto Tank = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());
	Tank->AddForceAtLocation(Force, ForceLocation);
}


