// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurret.h"
#include "Engine/World.h"
void UTankTurret::TurnTurret(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1, 1);
	auto TurnDegreeChange = RelativeSpeed * MaxDegreesPerSecond* (GetWorld()->DeltaTimeSeconds);
	auto TurnDegree = RelativeRotation.Yaw + TurnDegreeChange;
	//auto TurnDegree = FMath::Clamp<float>(RawNewTurnDegree, MinTurnDegree, MaxTurnDegree);
	SetRelativeRotation(FRotator(0,TurnDegree,0));
}



