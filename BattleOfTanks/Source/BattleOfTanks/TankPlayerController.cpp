// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "Engine/World.h"
#include "TankAimingComponent.h"
ATankPlayerController::ATankPlayerController()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CrosshairXLocation = 0.5f;
	CrosshairYLocation = 0.33333f;
}
void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if(AimingComponent)
	{
		AimingComponentFound(AimingComponent);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerController cannot find aiming component at Begin Play "));
	}

}
void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AimTowardsCrosshair();
}

void ATankPlayerController::AimTowardsCrosshair()
{
	if (!GetPawn()) { return; }
	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (ensure(AimingComponent))
	{
		FVector HitLocation;//out parameter 
		if(GetSightRayHitLocation(HitLocation))// has side effects,is going to line trace
		{
			AimingComponent->AimAt(HitLocation);
		}
	}
}
//get world location if the linetrace through crosshair , true if hits landscape
bool ATankPlayerController::GetSightRayHitLocation(FVector& HitLocation) const
{
		int32 ViewportSizeX, ViewportSizeY;
		FVector2D ScreenLocation;
		GetViewportSize(ViewportSizeX, ViewportSizeY);
		ScreenLocation = FVector2D((ViewportSizeX * CrosshairXLocation), (ViewportSizeY * CrosshairYLocation));
		FVector LookDirection;

		if (GetLookDirection(ScreenLocation, LookDirection))
		{
			FVector LineTraceEnd = FVector(ScreenLocation.X, ScreenLocation.Y, 0.f) + LookDirection * 100000.f;
			if (GetLookVectorHitLocation(HitLocation, LookDirection))
			{
				return true;
			}
			else { return false; }
		}
		else { return false; }
}
bool ATankPlayerController::GetLookVectorHitLocation(FVector& HitLocation, FVector LookDirection) const
{
	FHitResult HitResult;
	auto StartPos = PlayerCameraManager->GetCameraLocation();
	auto EndPos = StartPos + (LookDirection * LineTraceRange);

	if (GetWorld()->LineTraceSingleByChannel(
		HitResult,
		StartPos,
		EndPos,
		ECollisionChannel::ECC_Visibility))
	{
		HitLocation = HitResult.Location;
		return true;
	}
	else
	{
		return false;
	}
}
bool ATankPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const
{
	FVector CameraWorldLocation;
	return DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CameraWorldLocation, LookDirection);
}


FVector ATankPlayerController::GetLineTraceEnd()
{
	return FVector();
}
